#!/bin/bash

usage() {
    echo "Usage: $0  ‹Destination Folder› ( ‹pluzz.francetv.fr URL› )⁺"
    echo "Requires: wget."
    exit 1
}
[[ $# -lt 2 ]] && usage

print () {
    #return # Sets verbose off.
    echo -ne "\033[01;34m$1"
    shift 1
    echo -e " :: \033[00m\033[01;01m$*\033[00m"
}

trim_sides() {
    [[ $# -ne 3 ]] && exit 1
    overhead=$1
    overfoot=$3
    word=$2
    echo ${word:${#overhead}:$((- ${#overhead} + ${#word} - ${#overfoot}))}
}

UA="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.47 Safari/536.11"
get() {
    file="$1"; shift 1
    wget -O $file --user-agent="${UA}" $*
    if [ ! $? -eq 0 ]; then
        print 'ACCESS DENIED' $1
        exit 3
    fi
}


D="$1"
mkdir -p $D
print "Destination Folder" $D
shift

until [ "$1" = '' ]; do
    tmp="$D"/pluzz_$RANDOM
    mkdir $tmp
    if [ ! $? -eq 0 ]; then
        echo "No puedo generar $tmp"
        exit 1
    fi
    F=$tmp/buff
    pwd=`pwd`
    cd $tmp

    vid=''
    name=pluzz_$RANDOM
    echo $1 | grep -Eo 'pluzz\.francetv\.fr/videos/[^/]+html$' 1>/dev/null
    if [ $? -eq 0 ]; then
        print "Starting page" $1
        get $F $1
        vid=$( cat $F | grep -Po 'id-video=\d+"' )
        vid=$( trim_sides 'id-video=' "$vid" '"' )
        name=$( cat $F | grep -Eo '"og:title" content="France 2 en replay \| [^"]+"' )
        name=$( trim_sides '"og:title" content="France 2 en replay | ' "$name" '"' )
    else
        echo $1 | grep -Eo 'www\.france.+/' 1>/dev/null
        if [ $? -eq 0 ]; then
            print "Starting page" $1
            get $F $1
            vid=$( cat $F | grep -Po 'fr/videos/\d+"' )
            vid=$( trim_sides 'fr/videos/' "$vid" '"' )
            name=$( cat $F | grep -Eo '"og:title" content="Vidéo - [^"]+"' )
            name=$( trim_sides '"og:title" content="Vidéo - ' "$name" '"' )
        else
            echo $1 | grep -Eo '^[0-9]+$' 1>/dev/null
            if [ $? -eq 0 ]; then
                vid=$1
            else
                echo "Pas pu accéder à la vidéo"
                echo "…ou alors l'ordre des arguments est mauvais"
                usage
            fi
        fi
    fi
    print "Émission" $name
    print "id-video" $vid
    [[ "$vid" = '' ]] && echo "Couldn't find video's ID. Sorry…" && exit 2

    oeuvre='http://pluzz.francetv.fr/appftv/webservices/video/getInfosOeuvre.php?mode=zeri&id-diffusion='$vid
    print "XML infos" $oeuvre
    get $F $oeuvre

    m3uPath=http://medias2.francetv.fr/catchup-mobile/france-dom-tom/non-token/non-drm/m3u8
    m3u8s=$( cat $F | grep -Eo '[0-9]{4}/S[0-9]{2}/J[0-9]{1}/[0-9]*-[0-9]{6,8}' )
    m3u8s=$m3uPath/$m3u8s.m3u8
    print "List of M3U8s at" $m3u8s
    get $F $m3u8s

    bestQual=$( tail -n 1 $F )
    bestQual=$( trim_sides '' "$bestQual" '\r' )8 #done weird shit to get rid of \r
    looseEnd=$( echo $m3u8s | grep -Eo '/[^/]+$' )
    url=$( trim_sides '' "$m3u8s" "$looseEnd" )/$bestQual
    print "Choosen M3U8" $url
    get $F $url

    ts=$( trim_sides '' "$m3u8s" "$looseEnd" )
    #buff=''
    while read line; do
        if [ "${line:0:1}" != '#' ]; then
            #buff=${buff}"$ts/$line\n"
            echo "$ts/$line" >> list
        fi
    done < $F
    #echo -e "$buff" > list

    print "Fetching a lot of TS files found" $url
    wget -i list --user-agent="${UA}"

    prefix=$( trim_sides '/' $looseEnd '.m3u8' )
    salt=$RANDOM
    cat $prefix-?.ts    > ${salt}_a.ts && rm -f $prefix-?.ts
    cat $prefix-??.ts   > ${salt}_b.ts && rm -f $prefix-??.ts
    cat $prefix-???.ts  > ${salt}_c.ts && rm -f $prefix-???.ts
    cat $prefix-????.ts > ${salt}_d.ts && rm -f $prefix-????.ts # To be sure
    cat ${salt}_?.ts > final           && rm -f ${salt}_?.ts

    cd $pwd
    fn="${D}/${name}".ts
    mv $tmp/final "$fn"
    rm -rf $tmp

    print "File Saved at" "$fn"
    echo -e "$vid\t$(date)\t$url" >> ~/.dled

    shift
done
