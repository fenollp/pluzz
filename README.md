# PLUZZ videos backup
A UNIX Shell script that accepts command-line requests to download
`pluzz.francetv.fr`'s videos.

## Synopsis

    :::bash
    ./francetv.sh  ‹Destination Folder› ⦉ ‹pluzz.francetv.fr URL› ⦊⁺

### Case Study

Here you have a show you would like to backup, pick its address. It should look
like `http://pluzz.francetv.fr/videos/ce_soir_ou_jamais.html`.

Download the show with:

    :::bash
    ./francetv.sh ~/Downloads http://pluzz.francetv.fr/videos/ce_soir_ou_jamais.html


## Requirements
* wget
